﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DNPatients.Models;
using Microsoft.AspNetCore.Http;
using System;

namespace DNPatients.Controllers
{
    public class DNPatientTreatmentsController : Controller
    {
        private readonly PatientsContext _context;

        public DNPatientTreatmentsController(PatientsContext context)
        {
            _context = context;
        }

        // GET: DNPatientTreatments
        public async Task<IActionResult> Index(string patientDiagnosisId)
        {

            if(patientDiagnosisId != null)
            {
                HttpContext.Session.SetString("patientDiagnosisId", patientDiagnosisId);
                ViewData["PDID"] = patientDiagnosisId;
            }
            else if(Request.Query["patientDiagnosisId"].Any())
            {
                HttpContext.Session.SetString("patientDiagnosisId", Request.Query["patientDiagnosisId"]);
                ViewData["PDID"] = patientDiagnosisId;
            }
            else if (HttpContext.Session.GetString("patientDiagnosisId") != null)
            {
                patientDiagnosisId = HttpContext.Session.GetString("patientDiagnosisId");
                ViewData["PDID"] = patientDiagnosisId;
            }
            else
            {
                TempData["Message"] = "Please select Patients Diagnosis first!";
                return RedirectToAction("Index", "DNPatientDiagnosis");
            }
            var Pcontext = _context.PatientTreatment.Include(p => p.PatientDiagnosis).Include(p=>p.Treatment).Where(p => p.PatientDiagnosisId == 3).FirstOrDefault();
            ViewData["PatientsDiagID"] = Pcontext.PatientDiagnosisId;
            GetPatientsNameTreatment();
            var patientsContext = _context.PatientTreatment.Include(p => p.PatientDiagnosis).Include(p => p.Treatment).Where(p => p.PatientDiagnosisId == Convert.ToInt32(HttpContext.Session.GetString("patientDiagnosisId"))).OrderByDescending(p =>p.DatePrescribed);
            return View(await patientsContext.ToListAsync());
        }

        public void GetPatientsNameTreatment ()
        {
            string PnameTreatment = string.Empty;
            if (Request.Query["patientDiagnosisId"].Any())
            {
                HttpContext.Session.SetString("patientDiagnosisId", Request.Query["patientDiagnosisId"]);
            }
           else if (HttpContext.Session.GetString("patientDiagnosisId") != null)
            {
                PnameTreatment = HttpContext.Session.GetString("patientDiagnosisId");
            }
            var namecontext = _context.PatientDiagnosis.Include(p =>p.Diagnosis).Include(p=>p.Patient).Where(p => p.PatientDiagnosisId == Convert.ToInt32(HttpContext.Session.GetString("patientDiagnosisId"))).FirstOrDefault();
           // var Pcontext = _context.PatientTreatment.Where(p => p.PatientDiagnosisId == Convert.ToInt32(HttpContext.Session.GetString("patientDiagnosisId"))).FirstOrDefault();
            ViewData["PatientsName"] = namecontext.Patient.LastName + ", " + namecontext.Patient.FirstName;
            ViewData["DiagName"] = namecontext.Diagnosis.Name;

        }
        // GET: DNPatientTreatments/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            GetPatientsNameTreatment();
            if (id == null)
            {
                return NotFound();
            }

            var patientTreatment = await _context.PatientTreatment
                .Include(p => p.PatientDiagnosis)
                .Include(p => p.Treatment)
                .FirstOrDefaultAsync(m => m.PatientTreatmentId == id);
            if (patientTreatment == null)
            {
                return NotFound();
            }

            return View(patientTreatment);
        }

        // GET: DNPatientTreatments/Create
        public IActionResult Create()
        {
            string PDiagID = string.Empty;
            if (Request.Query["PatientsDiagID"].Any())
            {
                HttpContext.Session.SetString("patientDiagnosisId", Request.Query["PatientsDiagID"]);
            }
            else if (HttpContext.Session.GetString("patientDiagnosisId") != null)
            {
                PDiagID = HttpContext.Session.GetString("patientDiagnosisId");
            }
          
            var Pcontext = _context.PatientTreatment.Where(p => p.PatientDiagnosisId == Convert.ToInt32(HttpContext.Session.GetString("patientDiagnosisId"))).FirstOrDefault();
         //   ViewData["PatientsDiagID"] = Pcontext.PatientDiagnosisId;
            GetPatientsNameTreatment();

            ViewData["PatientDiagnosisId"] = new SelectList(_context.PatientDiagnosis, "PatientDiagnosisId", "PatientDiagnosisId");
            ViewData["TreatmentId"] = new SelectList(_context.Treatment.Where(p => p.DiagnosisId == Convert.ToInt32(HttpContext.Session.GetString("patientDiagnosisId"))), "TreatmentId", "Name");
            return View();
        }

        // POST: DNPatientTreatments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PatientTreatmentId,TreatmentId,DatePrescribed,Comments,PatientDiagnosisId")] PatientTreatment patientTreatment)
        {
            if (ModelState.IsValid)
            {
                _context.Add(patientTreatment);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["PatientDiagnosisId"] = new SelectList(_context.PatientDiagnosis, "PatientDiagnosisId", "PatientDiagnosisId", patientTreatment.PatientDiagnosisId);
            ViewData["TreatmentId"] = new SelectList(_context.Treatment, "TreatmentId", "Name");
            return View(patientTreatment);
        }

        // GET: DNPatientTreatments/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            GetPatientsNameTreatment();
            string PDiagID = string.Empty;
            if (HttpContext.Session.GetString("patientDiagnosisId") != null)
            {
                PDiagID = HttpContext.Session.GetString("patientDiagnosisId");
            }
            var Pcontext = _context.PatientTreatment.Where(p => p.PatientDiagnosisId == Convert.ToInt32(PDiagID)).FirstOrDefault();
            ViewData["PatientsDiagID"] = Pcontext.PatientDiagnosisId;
            //ViewData["DiagName"] = Pcontext.PatientDiagnosis.Diagnosis.Name;
            if (id == null)
            {
                return NotFound();
            }

            var patientTreatment = await _context.PatientTreatment.FindAsync(id);
            if (patientTreatment == null)
            {
                return NotFound();
            }
            
            ViewData["PatientDiagnosisId"] = new SelectList(_context.PatientDiagnosis, "PatientDiagnosisId", "PatientDiagnosisId", patientTreatment.PatientDiagnosisId);
            ViewData["TreatmentId"] = new SelectList(_context.Treatment.Where(p => p.DiagnosisId == Convert.ToInt32(PDiagID)), "TreatmentId", "Name");
            return View(patientTreatment);
        }

        // POST: DNPatientTreatments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PatientTreatmentId,TreatmentId,DatePrescribed,Comments,PatientDiagnosisId")] PatientTreatment patientTreatment)
        {
            if (id != patientTreatment.PatientTreatmentId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(patientTreatment);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PatientTreatmentExists(patientTreatment.PatientTreatmentId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["PatientDiagnosisId"] = new SelectList(_context.PatientDiagnosis, "PatientDiagnosisId", "PatientDiagnosisId", patientTreatment.PatientDiagnosisId);
            ViewData["TreatmentId"] = new SelectList(_context.Treatment, "TreatmentId", "Name", patientTreatment.TreatmentId);
            return View(patientTreatment);
        }

        // GET: DNPatientTreatments/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            GetPatientsNameTreatment();
            if (id == null)
            {
                return NotFound();
            }

            var patientTreatment = await _context.PatientTreatment
                .Include(p => p.PatientDiagnosis)
                .Include(p => p.Treatment)
                .FirstOrDefaultAsync(m => m.PatientTreatmentId == id);
            if (patientTreatment == null)
            {
                return NotFound();
            }

            return View(patientTreatment);
        }

        // POST: DNPatientTreatments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            GetPatientsNameTreatment();
            var patientTreatment = await _context.PatientTreatment.FindAsync(id);
            _context.PatientTreatment.Remove(patientTreatment);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PatientTreatmentExists(int id)
        {
            return _context.PatientTreatment.Any(e => e.PatientTreatmentId == id);
        }
    }
}
