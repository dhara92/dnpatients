﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DNPatients.Models;

namespace DNPatients.Controllers
{
    public class DNCountriesController : Controller
    {
        private readonly PatientsContext _context;

        public DNCountriesController(PatientsContext context)
        {
            _context = context;
        }

        /*GET: DNCountries
         * Displays all the records in Counrty table 
         */
        
        public async Task<IActionResult> Index()
        {
            return View(await _context.Country.ToListAsync());
        }

        /* GET: DNCountries/Details/5
         * it render Display.cshtml and shows the details for selected table
         */
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var country = await _context.Country
                .FirstOrDefaultAsync(m => m.CountryCode == id);
            if (country == null)
            {
                return NotFound();
            }

            return View(country);
        }

        /* GET: DNCountries/Create
         * it render create.cshtml
        */
        public IActionResult Create()
        {
            return View();
        }

        /* POST: DNCountries/Create
         * It is exexute when we press create button and create the reacord in to table too
         * after that it redirct to index page(main page for country table).
         * To protect from overposting attacks, please enable the specific properties you want to bind to, for 
         * more details see http://go.microsoft.com/fwlink/?LinkId=317598.
         */
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CountryCode,Name,PostalPattern,PhonePattern,FederalSalesTax")] Country country)
        {
            if (ModelState.IsValid)
            {
                _context.Add(country);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(country);
        }

        /* GET: DNCountries/Edit/5
         *it render the edit page  with selected counrty information
         *
         */
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var country = await _context.Country.FindAsync(id);
            if (country == null)
            {
                return NotFound();
            }
            return View(country);
        }

        /* POST: DNCountries/Edit/5
         * it edit the record and save changes in actual table and after that renders main page
         * for country.
         * To protect from overposting attacks, please enable the specific properties you want to bind to, for 
         * more details see http://go.microsoft.com/fwlink/?LinkId=317598.
         */
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("CountryCode,Name,PostalPattern,PhonePattern,FederalSalesTax")] Country country)
        {
            if (id != country.CountryCode)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(country);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CountryExists(country.CountryCode))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(country);
        }

        /* GET: DNCountries/Delete/5
         * it render delete.cshtml for confirmation weather sure to delete record or not
         */
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var country = await _context.Country
                .FirstOrDefaultAsync(m => m.CountryCode == id);
            if (country == null)
            {
                return NotFound();
            }

            return View(country);
        }

        /* POST: DNCountries/Delete/5
         * it delete the record from table and redirect to main page of country table(index.cshtml)
        */
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var country = await _context.Country.FindAsync(id);
            _context.Country.Remove(country);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        //checks weather country is exit or not by using primaty key column
        private bool CountryExists(string id)
        {
            return _context.Country.Any(e => e.CountryCode == id);
        }
    }
}
