﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DNPatients.Models;
using Microsoft.EntityFrameworkCore.Internal;
using System;

namespace DNPatients.Controllers
{
    public class DNPatientDiagnosisController : Controller
    {
        private readonly PatientsContext _context;

        public DNPatientDiagnosisController(PatientsContext context)
        {
            _context = context;
        }

        // GET: DNPatientDiagnosis
        public async Task<IActionResult> Index(string PatientsID)
        {
           
            if (PatientsID != null)
            {
                HttpContext.Session.SetString("PatientsID", PatientsID);
                
            }
            else if (Request.Query["PatientId"].Any())
            {
                HttpContext.Session.SetString("PatientsID", Request.Query["PatientId"]);
            }
            else if (HttpContext.Session.GetString("PatientsID")!= null)
            {
                PatientsID = HttpContext.Session.GetString("PatientsID");
            }
            else
            {
                TempData["Message"] = "Please select Patient to proceed!";
                return RedirectToAction("Index", "DNPatients");
            }

            var pContext = _context.Patient.Where(p => p.PatientId == Convert.ToInt32(HttpContext.Session.GetString("PatientsID"))).FirstOrDefault();
            ViewData["PatientName"] = pContext.LastName+", "+ pContext.FirstName;

            var patientsContext = _context.PatientDiagnosis.Include(p => p.Diagnosis).Include(p => p.Patient).Include(p => p.PatientTreatment).Where(p => p.PatientId == Convert.ToInt32(HttpContext.Session.GetString("PatientsID"))).OrderBy(p => p.Patient.LastName).ThenBy(p => p.Patient.FirstName).ThenByDescending(p =>p.PatientDiagnosisId); 
            return View(await patientsContext.ToListAsync());
        }

        public void getPatientsName()
        {
            string PatientsID = string.Empty;
            if (Request.Query["PatientId"].Any())
            {
                HttpContext.Session.SetString("PatientsID", Request.Query["PatientId"]);
            }
            else if (HttpContext.Session.GetString("PatientsID") != null)
            {
                PatientsID = HttpContext.Session.GetString("PatientsID");
            }

            var pContext = _context.Patient.Where(p => p.PatientId == Convert.ToInt32(HttpContext.Session.GetString("PatientsID"))).FirstOrDefault();
            ViewData["PatientName"] = pContext.LastName + ", " + pContext.FirstName;
        }
        // GET: DNPatientDiagnosis/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            getPatientsName();
            if (id == null)
            {
                return NotFound();
            }

            var patientDiagnosis = await _context.PatientDiagnosis
                .Include(p => p.Diagnosis)
                .Include(p => p.Patient)
                .FirstOrDefaultAsync(m => m.PatientDiagnosisId == id);
            if (patientDiagnosis == null)   
            {
                return NotFound();
            }

            return View(patientDiagnosis);
        }

        // GET: DNPatientDiagnosis/Create
        public IActionResult Create()
        {
            getPatientsName();
            ViewData["DiagnosisId"] = new SelectList(_context.Diagnosis, "DiagnosisId", "Name");
            ViewData["PatientId"] = new SelectList(_context.Patient, "PatientId", "FirstName");
            return View();
        }

        // POST: DNPatientDiagnosis/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PatientDiagnosisId,PatientId,DiagnosisId,Comments")] PatientDiagnosis patientDiagnosis)
        {
            if (ModelState.IsValid)
            {
                _context.Add(patientDiagnosis);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["DiagnosisId"] = new SelectList(_context.Diagnosis, "DiagnosisId", "Name", patientDiagnosis.DiagnosisId);
            ViewData["PatientId"] = new SelectList(_context.Patient, "PatientId", "FirstName", patientDiagnosis.PatientId);
            return View(patientDiagnosis);
        }

        // GET: DNPatientDiagnosis/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            getPatientsName();
            if (id == null)
            {
                return NotFound();
            }

            var patientDiagnosis = await _context.PatientDiagnosis.FindAsync(id);
            if (patientDiagnosis == null)
            {
                return NotFound();
            }
            ViewData["DiagnosisId"] = new SelectList(_context.Diagnosis, "DiagnosisId", "Name", patientDiagnosis.DiagnosisId);
            ViewData["PatientId"] = new SelectList(_context.Patient, "PatientId", "FirstName", patientDiagnosis.PatientId);
            return View(patientDiagnosis);
        }

        // POST: DNPatientDiagnosis/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PatientDiagnosisId,PatientId,DiagnosisId,Comments")] PatientDiagnosis patientDiagnosis)
        {
            if (id != patientDiagnosis.PatientDiagnosisId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(patientDiagnosis);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PatientDiagnosisExists(patientDiagnosis.PatientDiagnosisId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["DiagnosisId"] = new SelectList(_context.Diagnosis, "DiagnosisId", "Name", patientDiagnosis.DiagnosisId);
            ViewData["PatientId"] = new SelectList(_context.Patient, "PatientId", "FirstName", patientDiagnosis.PatientId);
            return View(patientDiagnosis);
        }

        // GET: DNPatientDiagnosis/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            getPatientsName();
            if (id == null)
            {
                return NotFound();
            }

            var patientDiagnosis = await _context.PatientDiagnosis
                .Include(p => p.Diagnosis)
                .Include(p => p.Patient)
                .FirstOrDefaultAsync(m => m.PatientDiagnosisId == id);
            if (patientDiagnosis == null)
            {
                return NotFound();
            }

            return View(patientDiagnosis);
        }

        // POST: DNPatientDiagnosis/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var patientDiagnosis = await _context.PatientDiagnosis.FindAsync(id);
            _context.PatientDiagnosis.Remove(patientDiagnosis);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PatientDiagnosisExists(int id)
        {
            return _context.PatientDiagnosis.Any(e => e.PatientDiagnosisId == id);
        }
    }
}
