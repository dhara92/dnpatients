﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DNPatients.Models;

namespace DNPatients.Controllers
{
    public class DNDispensingUnitsController : Controller
    {
        private readonly PatientsContext _context;

        public DNDispensingUnitsController(PatientsContext context)
        {
            _context = context;
        }

        /* GET: DNDispensingUnits
         * it render index.cshtml to list all the dispensing unit from the table.
         */
        public async Task<IActionResult> Index()
        {
            return View(await _context.DispensingUnit.ToListAsync());
        }

        /* GET: DNDispensingUnits/Details/5
         * It dispalys all the details about particular dispensing unit and for that it render 
         * Details.cshtml
         */
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dispensingUnit = await _context.DispensingUnit
                .FirstOrDefaultAsync(m => m.DispensingCode == id);
            if (dispensingUnit == null)
            {
                return NotFound();
            }

            return View(dispensingUnit);
        }

        /* GET: DNDispensingUnits/Create
         * it renders Create.cshtml to allow user to input the values for the new unit
         */
        public IActionResult Create()
        {
            return View();
        }

        /* POST: DNDispensingUnits/Create
         * after input all the information,it create new record in to table and displays index.cshtml
         * with new records.
         * To protect from overposting attacks, please enable the specific properties you want to bind to, for 
         * more details see http://go.microsoft.com/fwlink/?LinkId=317598.
         */
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("DispensingCode")] DispensingUnit dispensingUnit)
        {
            if (ModelState.IsValid)
            {
                _context.Add(dispensingUnit);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(dispensingUnit);
        }

        /* GET: DNDispensingUnits/Edit/5
         * it display Edit.cshtml to allow particular record editing to the user.
         */
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dispensingUnit = await _context.DispensingUnit.FindAsync(id);
            if (dispensingUnit == null)
            {
                return NotFound();
            }
            return View(dispensingUnit);
        }

        /* POST: DNDispensingUnits/Edit/5
         * on pressinn edit button, it update the selected record in to table and render the index.cshtml 
         * with successful updation.
         * To protect from overposting attacks, please enable the specific properties you want to bind to, for 
         * more details see http://go.microsoft.com/fwlink/?LinkId=317598.
         */
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("DispensingCode")] DispensingUnit dispensingUnit)
        {
            if (id != dispensingUnit.DispensingCode)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(dispensingUnit);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DispensingUnitExists(dispensingUnit.DispensingCode))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(dispensingUnit);
        }

        /* GET: DNDispensingUnits/Delete/5
         * It render Delete.cshtml for the confimation with the selected record.
         */
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dispensingUnit = await _context.DispensingUnit
                .FirstOrDefaultAsync(m => m.DispensingCode == id);
            if (dispensingUnit == null)
            {
                return NotFound();
            }

            return View(dispensingUnit);
        }

        /*POST: DNDispensingUnits/Delete/5
         *It delete the record from the table with corrensponding id and after succesful deletion
         * it render index.cshtml
         */
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var dispensingUnit = await _context.DispensingUnit.FindAsync(id);
            _context.DispensingUnit.Remove(dispensingUnit);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        
        //it is used to check weather record is exists or not by primary key column name       
        private bool DispensingUnitExists(string id)
        {
            return _context.DispensingUnit.Any(e => e.DispensingCode == id);
        }
    }
}
