﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DNPatients.Models;
using RestSharp;
using System;
using FluentNHibernate.Conventions.Inspections;

namespace DNPatients.Controllers
{
   
    public class DNMedicationsController : Controller
    {
      
        private readonly PatientsContext _context;

        public DNMedicationsController(PatientsContext context)
        {
            _context = context;
        }

        // GET: DNMedications
        public async Task<IActionResult> Index(string MedicationTypeId)
        {
            if (MedicationTypeId != null)
            {
                HttpContext.Session.SetString("MedTypeID", MedicationTypeId);
            }
            else if (Request.Query["MedTypeID"].Any())
            {
                HttpContext.Session.SetString("MedTypeID", Request.Query["MedTypeID"]);
            }
            else if (HttpContext.Session.GetString("MedTypeID") != null)
            {
                MedicationTypeId = HttpContext.Session.GetString("MedTypeID");
            }
            else
            {
                TempData["Message"] = "Please Select Medication Type  First";
                return RedirectToAction("Index", "DNMedicationTypes");
            }
            var context = _context.MedicationType.Where(m => m.MedicationTypeId == Convert.ToInt32(HttpContext.Session.GetString("MedTypeID"))).FirstOrDefault();
            ViewData["MedID"] = context.MedicationTypeId;
            ViewData["MedName"] = context.Name;

            var patientsContext = _context.Medication.Include(m => m.ConcentrationCodeNavigation).Include(m => m.DispensingCodeNavigation).Include(m => m.MedicationType).Where(m => m.MedicationTypeId == Convert.ToInt32(HttpContext.Session.GetString("MedTypeID"))).OrderBy(m => m.Name).ThenBy(m => m.Concentration);
            return View(await patientsContext.ToListAsync());
        }

        public void GetMedicationTypeName()
        {
            string medType = string.Empty;

            if (HttpContext.Session.GetString("MedTypeID") != null)
            {
                medType = HttpContext.Session.GetString("MedTypeID");
            }
            var context = _context.MedicationType.Where(m => m.MedicationTypeId == Convert.ToInt32(medType)).FirstOrDefault();
            ViewData["MedID"] = context.MedicationTypeId;
            ViewData["MedName"] = context.Name;
        }
        // GET: DNMedications/Details/5
        public async Task<IActionResult> Details(string id)
        {
            
            if (id == null)
            {
                return NotFound();
            }

            var medication = await _context.Medication
                .Include(m => m.ConcentrationCodeNavigation)
                .Include(m => m.DispensingCodeNavigation)
                .Include(m => m.MedicationType)
                .FirstOrDefaultAsync(m => m.Din == id);
            if (medication == null)
            {
                return NotFound();
            }
            GetMedicationTypeName();

            return View(medication);
        }

        // GET: DNMedications/Create
        public IActionResult Create(int id)
        {
            ViewData["ConcentrationCode"] = new SelectList(_context.ConcentrationUnit, "ConcentrationCode", "ConcentrationCode");
            ViewData["DispensingCode"] = new SelectList(_context.DispensingUnit, "DispensingCode", "DispensingCode");
            ViewData["MedicationTypeId"] = new SelectList(_context.MedicationType, "MedicationTypeId", "Name", id);
            GetMedicationTypeName();
            return View();
        }

        // POST: DNMedications/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind(include:"Din,Name,Image,MedicationTypeId,DispensingCode,Concentration,ConcentrationCode")] Medication medication)
        {
            string medType = string.Empty;

            if (HttpContext.Session.GetString("MedTypeID") != null)
            {
                medType = HttpContext.Session.GetString("MedTypeID");
            }
            medication.MedicationTypeId = Convert.ToInt32(medType);

            var isDuplicateRecord = _context.Medication.Where(a => a.Concentration == medication.Concentration && a.ConcentrationCode == medication.ConcentrationCode && a.Name == medication.Name);
            if(isDuplicateRecord.Any())
            {
               
                ModelState.AddModelError("","Duplicate Record Found");
                TempData["Message"] = "Duplicate Record Found";
                return RedirectToAction(nameof(Create));
            }

            try
            {
                  if (ModelState.IsValid)
               
                        _context.Add(medication);
                        await _context.SaveChangesAsync();
                        TempData["Message"] = "New Medication Added!";
                        return RedirectToAction(nameof(Index)); 
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("", ex.GetBaseException().Message);

            }
            ViewData["ConcentrationCode"] = new SelectList(_context.ConcentrationUnit.OrderBy(m => m.ConcentrationCode), "ConcentrationCode", "ConcentrationCode", medication.ConcentrationCode);
            ViewData["DispensingCode"] = new SelectList(_context.DispensingUnit.OrderBy(m => m.DispensingCode), "DispensingCode", "DispensingCode", medication.DispensingCode);
            ViewData["MedicationTypeId"] = new SelectList(_context.MedicationType, "MedicationTypeId", "Name", medication.MedicationTypeId);
            return View(medication);
        }

        // GET: DNMedications/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var medication = await _context.Medication.FindAsync(id);
            if (medication == null)
            {
                return NotFound();
            }
            ViewData["ConcentrationCode"] = new SelectList(_context.ConcentrationUnit, "ConcentrationCode", "ConcentrationCode", medication.ConcentrationCode);
            ViewData["DispensingCode"] = new SelectList(_context.DispensingUnit, "DispensingCode", "DispensingCode", medication.DispensingCode);
            ViewData["MedicationTypeId"] = new SelectList(_context.MedicationType, "MedicationTypeId", "Name", medication.MedicationTypeId);
            GetMedicationTypeName();
            return View(medication);
        }

        // POST: DNMedications/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Din,Name,Image,MedicationTypeId,DispensingCode,Concentration,ConcentrationCode")] Medication medication)
        {
            if (id != medication.Din)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(medication);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MedicationExists(medication.Din))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ConcentrationCode"] = new SelectList(_context.ConcentrationUnit.OrderBy(m => m.ConcentrationCode), "ConcentrationCode", "ConcentrationCode", medication.ConcentrationCode);
            ViewData["DispensingCode"] = new SelectList(_context.DispensingUnit.OrderBy(m => m.DispensingCode), "DispensingCode", "DispensingCode", medication.DispensingCode);
            ViewData["MedicationTypeId"] = new SelectList(_context.MedicationType, "MedicationTypeId", "Name", medication.MedicationTypeId);
            return View(medication);
        }

        // GET: DNMedications/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var medication = await _context.Medication
                .Include(m => m.ConcentrationCodeNavigation)
                .Include(m => m.DispensingCodeNavigation)
                .Include(m => m.MedicationType)
                .FirstOrDefaultAsync(m => m.Din == id);
            if (medication == null)
            {
                return NotFound();
            }
            GetMedicationTypeName();
            return View(medication);
        }

        // POST: DNMedications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var medication = await _context.Medication.FindAsync(id);
            _context.Medication.Remove(medication);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MedicationExists(string id)
        {
            return _context.Medication.Any(e => e.Din == id);
        }
    }
}
