﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DNPatients.Models;
using Microsoft.AspNetCore.Http;

namespace DNPatients.Controllers
{
    public class DNMedicationTypesController : Controller
    {
        private readonly PatientsContext _context;
        

        public DNMedicationTypesController(PatientsContext context)
        {
            _context = context;
        }

        /* GET: DNMedicationTypes
         * IT render index.cshtml and shows lists of all medication types available in
         * the table.
         */
        public async Task<IActionResult> Index(int id, string message)
        {
           
            var MedTypes = _context.MedicationType.OrderBy(a => a.Name);

            return View(MedTypes);
        }
 
         /* GET: DNMedicationTypes/Details/5
         * it display Details.cshtml and shows all the avialble information of the particalr
         * medication type.
         */
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var medicationType = await _context.MedicationType
                .FirstOrDefaultAsync(m => m.MedicationTypeId == id);
            if (medicationType == null)
            {
                return NotFound();
            }

            return View(medicationType);
        }

        /* GET: DNMedicationTypes/Create
         * It render create.cshtml and allows user to input all information to create new medication 
         * type.
         */ 
         
        public IActionResult Create()
        {
            return View();
        }

        /* POST: DNMedicationTypes/Create
         * It create new medication type which has values inserted by user to the table 
         * and render index.cshmtl with new records.
         * To protect from overposting attacks, please enable the specific properties you want to bind to, for 
         * more details see http://go.microsoft.com/fwlink/?LinkId=317598.
         */
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("MedicationTypeId,Name")] MedicationType medicationType)
        {
            if (ModelState.IsValid)
            {
                _context.Add(medicationType);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(medicationType);
        }

        /* GET: DNMedicationTypes/Edit/5
         * It render Edit.cshtml to allow user to update the value for particular medication
         * type.
         */
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var medicationType = await _context.MedicationType.FindAsync(id);
            if (medicationType == null)
            {
                return NotFound();
            }
            return View(medicationType);
        }

        /* POST: DNMedicationTypes/Edit/5
         * it is used to update record with new values on the table and render index.cshtml with 
         * new changes.
         * To protect from overposting attacks, please enable the specific properties you want to bind to, for 
         * more details see http://go.microsoft.com/fwlink/?LinkId=317598.
         */
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("MedicationTypeId,Name")] MedicationType medicationType)
        {
            if (id != medicationType.MedicationTypeId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(medicationType);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MedicationTypeExists(medicationType.MedicationTypeId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(medicationType);
        }

        /* GET: DNMedicationTypes/Delete/5
         * It render Delete.cshtml to confirm weather user wants to delete the record or not.
         */
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var medicationType = await _context.MedicationType
                .FirstOrDefaultAsync(m => m.MedicationTypeId == id);
            if (medicationType == null)
            {
                return NotFound();
            }

            return View(medicationType);
        }

        /* POST: DNMedicationTypes/Delete/5
         * It delete actual record form the table and render index.cshtml with new list.
         */
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var medicationType = await _context.MedicationType.FindAsync(id);
            _context.MedicationType.Remove(medicationType);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        //It is used to check weather record is exists or not by using primary key column
        private bool MedicationTypeExists(int id)
        {
            return _context.MedicationType.Any(e => e.MedicationTypeId == id);
        }
    }
}
