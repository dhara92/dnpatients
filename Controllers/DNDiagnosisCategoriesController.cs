﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DNPatients.Models;

namespace DNPatients.Controllers
{
    public class DNDiagnosisCategoriesController : Controller
    {
        private readonly PatientsContext _context;

        public DNDiagnosisCategoriesController(PatientsContext context)
        {
            _context = context;
        }

        /* GET: DNDiagnosisCategories
         * this will render main page for lising all dignosis category from the table
         */
        public async Task<IActionResult> Index()
        {
            return View(await _context.DiagnosisCategory.ToListAsync());
        }

        /* GET: DNDiagnosisCategories/Details/5
         * it displays all details of selected category with rendering the Detalis.cshtml 
         */
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var diagnosisCategory = await _context.DiagnosisCategory
                .FirstOrDefaultAsync(m => m.Id == id);
            if (diagnosisCategory == null)
            {
                return NotFound();
            }

            return View(diagnosisCategory);
        }

        /* GET: DNDiagnosisCategories/Create
         * It render create.cshtml and allow user to input values to create new category. 
         */
        public IActionResult Create()
        {
            return View();
        }

        /* POST: DNDiagnosisCategories/Create
         * It will insert new record in the table with user inputs and render main page(index.cshtml).
         * To protect from overposting attacks, please enable the specific properties you want to bind to, for 
         * more details see http://go.microsoft.com/fwlink/?LinkId=317598.
         */
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name")] DiagnosisCategory diagnosisCategory)
        {
            if (ModelState.IsValid)
            {
                _context.Add(diagnosisCategory);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(diagnosisCategory);
        }

        /* GET: DNDiagnosisCategories/Edit/5
         * it will render edit.cshtml to edit the category
         */
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var diagnosisCategory = await _context.DiagnosisCategory.FindAsync(id);
            if (diagnosisCategory == null)
            {
                return NotFound();
            }
            return View(diagnosisCategory);
        }

        /* POST: DNDiagnosisCategories/Edit/5
         * it will update record in the table and render index.cshtml with updated values.
         * To protect from overposting attacks, please enable the specific 
         * properties you want to bind to, for 
         * more details see http://go.microsoft.com/fwlink/?LinkId=317598.
         */
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name")] DiagnosisCategory diagnosisCategory)
        {
            if (id != diagnosisCategory.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(diagnosisCategory);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DiagnosisCategoryExists(diagnosisCategory.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(diagnosisCategory);
        }

        /* GET: DNDiagnosisCategories/Delete/5
         * it display delete.cshtml to confirmation weather user
         * wants to delete record or not?
         */
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var diagnosisCategory = await _context.DiagnosisCategory
                .FirstOrDefaultAsync(m => m.Id == id);
            if (diagnosisCategory == null)
            {
                return NotFound();
            }

            return View(diagnosisCategory);
        }

        /* POST: DNDiagnosisCategories/Delete/5
         * it delete record after confirmation from the table and render
         * main page with saved changes.
         */
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var diagnosisCategory = await _context.DiagnosisCategory.FindAsync(id);
            _context.DiagnosisCategory.Remove(diagnosisCategory);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        //this is used to check weather record exists or not with primary key column
        private bool DiagnosisCategoryExists(int id)
        {
            return _context.DiagnosisCategory.Any(e => e.Id == id);
        }
    }
}
