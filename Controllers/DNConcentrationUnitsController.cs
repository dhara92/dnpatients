﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DNPatients.Models;

namespace DNPatients.Controllers
{
    public class DNConcentrationUnitsController : Controller
    {
        private readonly PatientsContext _context;

        public DNConcentrationUnitsController(PatientsContext context)
        {
            _context = context;
        }

        /* GET: DNConcentrationUnits
         * this function list all the units in the ConcentrationUnit Table
         */
        public async Task<IActionResult> Index()
        {
            return View(await _context.ConcentrationUnit.ToListAsync());
        }

        /* GET: DNConcentrationUnits/Details/5
         * This will display information about selected Concentration Unit
         */
        
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var concentrationUnit = await _context.ConcentrationUnit
                .FirstOrDefaultAsync(m => m.ConcentrationCode == id);
            if (concentrationUnit == null)
            {
                return NotFound();
            }

            return View(concentrationUnit);
        }

        /* GET: DNConcentrationUnits/Create
         * this will render create.cshtml and allow input for concentration Unit
         */
        public IActionResult Create()
        {
            return View();
        }

        /* POST: DNConcentrationUnits/Create
         * it will store the information in to actual table and render main page.
         * To protect from overposting attacks, please enable the specific properties you want to bind to, for 
         * more details see http://go.microsoft.com/fwlink/?LinkId=317598.
         */
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ConcentrationCode")] ConcentrationUnit concentrationUnit)
        {
            if (ModelState.IsValid)
            {
                _context.Add(concentrationUnit);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(concentrationUnit);
        }

        /*GET: DNConcentrationUnits/Edit/5
         *This will render Edit.cshtml for edit particular unit
         */
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var concentrationUnit = await _context.ConcentrationUnit.FindAsync(id);
            if (concentrationUnit == null)
            {
                return NotFound();
            }
            return View(concentrationUnit);
        }

        /* POST: DNConcentrationUnits/Edit/5
         * this will edit an information in table and render the main page if successful.
         * To protect from overposting attacks, please enable the specific properties you want to bind to, for 
         * more details see http://go.microsoft.com/fwlink/?LinkId=317598.
         */
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("ConcentrationCode")] ConcentrationUnit concentrationUnit)
        {
            if (id != concentrationUnit.ConcentrationCode)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(concentrationUnit);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ConcentrationUnitExists(concentrationUnit.ConcentrationCode))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(concentrationUnit);
        }

        /* GET: DNConcentrationUnits/Delete/5
         *this will render delete.cshtml for delete particualr unit 
         */
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var concentrationUnit = await _context.ConcentrationUnit
                .FirstOrDefaultAsync(m => m.ConcentrationCode == id);
            if (concentrationUnit == null)
            {
                return NotFound();
            }

            return View(concentrationUnit);
        }

        /* POST: DNConcentrationUnits/Delete/5
         * this will delete the record form table and save changes after that it render
         * index.cshtml page.
         */
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var concentrationUnit = await _context.ConcentrationUnit.FindAsync(id);
            _context.ConcentrationUnit.Remove(concentrationUnit);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        /* this will check weather the record is exists or not 
         * 
         */
        private bool ConcentrationUnitExists(string id)
        {
            return _context.ConcentrationUnit.Any(e => e.ConcentrationCode == id);
        }
    }
}
