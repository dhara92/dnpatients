﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DNPatients.Models
{
    [ModelMetadataType(typeof(PatientTreatmentMetadata))]
    public partial class PatientTreatment
    { }
    public class PatientTreatmentMetadata
    {
        public int PatientTreatmentId { get; set; }

        [Display(Name = "TreatmentId")]
        public int TreatmentId { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd MMMM yyyy HH:mm}")]
      
        public DateTime DatePrescribed { get; set; }

        public string Comments { get; set; }
        public int PatientDiagnosisId { get; set; }
    }
}
