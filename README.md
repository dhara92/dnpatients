Hi there! This is Dhara Narola

This is a .Net 2.2 MVC website for Clinic Management.

-To deploy the project Download repository and extract it.

-Execute Patients.sql file in to SSMS(SQL Server Management Studio) to create database and keep it running.

-Open DNPatients.sln file in to Visual Studio 2017 and later version.

-Open appsetting.json file and modify the server name for "DNPatientsConnection" string like following.

  DNPatientsConnection: "Server=[Your Server Name];Database=Patients;Trusted_Connection=True"

-That's it!..
 
 -License File
  I use GNU General Public License v3.0 becouse i believe in sharing and distribution
  of knowledge. This project is only for educational purpose.
 
